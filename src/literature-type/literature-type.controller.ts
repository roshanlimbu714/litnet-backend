import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { LiteratureTypeService } from './literature-type.service';
import { CreateLiteratureTypeDto } from './dto/create-literature-type.dto';
import { UpdateLiteratureTypeDto } from './dto/update-literature-type.dto';

@Controller('literature-type')
export class LiteratureTypeController {
  constructor(private readonly literatureTypeService: LiteratureTypeService) {}

  @Post()
  create(@Body() createLiteratureTypeDto: CreateLiteratureTypeDto) {
    return this.literatureTypeService.create(createLiteratureTypeDto);
  }

  @Get()
  findAll() {
    return this.literatureTypeService.findAll();
  }

  @Get('name/:name')
  findAllByName(@Param('name') name: string) {
    return this.literatureTypeService.findAllByName(name);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.literatureTypeService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLiteratureTypeDto: UpdateLiteratureTypeDto) {
    return this.literatureTypeService.update(+id, updateLiteratureTypeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.literatureTypeService.remove(+id);
  }
}
