import { Test, TestingModule } from '@nestjs/testing';
import { LiteratureTypeController } from './literature-type.controller';
import { LiteratureTypeService } from './literature-type.service';

describe('LiteratureTypeController', () => {
  let controller: LiteratureTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LiteratureTypeController],
      providers: [LiteratureTypeService],
    }).compile();

    controller = module.get<LiteratureTypeController>(LiteratureTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
