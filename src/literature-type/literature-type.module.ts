import { Module } from '@nestjs/common';
import { LiteratureTypeService } from './literature-type.service';
import { LiteratureTypeController } from './literature-type.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LiteratureType } from './entities/literature-type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([LiteratureType])],
  controllers: [LiteratureTypeController],
  providers: [LiteratureTypeService],
})
export class LiteratureTypeModule {}
