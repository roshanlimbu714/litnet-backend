import { PartialType } from '@nestjs/swagger';
import { CreateLiteratureTypeDto } from './create-literature-type.dto';

export class UpdateLiteratureTypeDto extends PartialType(CreateLiteratureTypeDto) {}
