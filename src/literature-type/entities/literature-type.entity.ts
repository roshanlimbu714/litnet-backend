import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Literature } from '../../literature/entities/literature.entity';

@Entity()
export class LiteratureType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @CreateDateColumn()
  createdAt: Date;

  @OneToMany(() => Literature, (lit) => lit.type, { nullable: true })
  literature: Literature[];

  @UpdateDateColumn()
  updatedAt: Date;
}
