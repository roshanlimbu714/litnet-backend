import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LiteratureType } from './entities/literature-type.entity';
import { CreateLiteratureTypeDto } from './dto/create-literature-type.dto';
import { UpdateLiteratureTypeDto } from './dto/update-literature-type.dto';

@Injectable()
export class LiteratureTypeService {
  constructor(
    @InjectRepository(LiteratureType)
    private literatureTypeRepository: Repository<LiteratureType>,
  ) {}

  async create(createLiteratureTypeDto: CreateLiteratureTypeDto) {
    return await this.literatureTypeRepository.save(createLiteratureTypeDto);
  }

  async findAll() {
    return await this.literatureTypeRepository
      .createQueryBuilder('lit')
      .getMany();
  }

  async findAllByName(name) {
    return await this.literatureTypeRepository
      .createQueryBuilder('lit')
      .where('lit.title=' + name)
      .leftJoinAndSelect('lit.literatures', 'literatures')
      .leftJoinAndSelect('lit.user', 'user')
      .getOne();
  }

  findOne(id: number) {
    return this.literatureTypeRepository
      .createQueryBuilder('lit')
      .where('lit.id=' + id)
      .getOne();
  }

  update(id: number, updateLiteratureTypeDto: UpdateLiteratureTypeDto) {
    return this.literatureTypeRepository.update(id, updateLiteratureTypeDto);
  }

  remove(id: number) {
    return this.literatureTypeRepository.delete(id);
  }
}
