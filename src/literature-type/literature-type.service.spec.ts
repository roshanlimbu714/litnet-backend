import { Test, TestingModule } from '@nestjs/testing';
import { LiteratureTypeService } from './literature-type.service';

describe('LiteratureTypeService', () => {
  let service: LiteratureTypeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LiteratureTypeService],
    }).compile();

    service = module.get<LiteratureTypeService>(LiteratureTypeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
