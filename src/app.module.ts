import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RolesModule } from './roles/roles.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from "./users/users.module";
import { UserDetailsModule } from "./user-details/user-details.module";
import { AuthModule } from "./auth/auth.module";
import { CommentsModule } from './comments/comments.module';
import { LiteratureModule } from './literature/literature.module';
import { LiteratureTypeModule } from './literature-type/literature-type.module';
import { RatingModule } from './rating/rating.module';
import { ImageModule } from './image/image.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      database: 'litnet',
      username: 'root',
      password: 'Vfr42wsX',
      port: 3306,
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    UsersModule,
    UserDetailsModule,
    AuthModule,
    RolesModule,
    CommentsModule,
    LiteratureModule,
    LiteratureTypeModule,
    RatingModule,
    ImageModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
