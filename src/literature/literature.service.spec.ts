import { Test, TestingModule } from '@nestjs/testing';
import { LiteratureService } from './literature.service';

describe('LiteratureService', () => {
  let service: LiteratureService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LiteratureService],
    }).compile();

    service = module.get<LiteratureService>(LiteratureService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
