import { Test, TestingModule } from '@nestjs/testing';
import { LiteratureController } from './literature.controller';
import { LiteratureService } from './literature.service';

describe('LiteratureController', () => {
  let controller: LiteratureController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LiteratureController],
      providers: [LiteratureService],
    }).compile();

    controller = module.get<LiteratureController>(LiteratureController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
