import { PartialType } from '@nestjs/swagger';
import { CreateLiteratureDto } from './create-literature.dto';

export class UpdateLiteratureDto extends PartialType(CreateLiteratureDto) {}
