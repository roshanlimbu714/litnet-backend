import { Module } from '@nestjs/common';
import { LiteratureService } from './literature.service';
import { LiteratureController } from './literature.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Literature } from './entities/literature.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Literature])],
  controllers: [LiteratureController],
  providers: [LiteratureService],
})
export class LiteratureModule {}
