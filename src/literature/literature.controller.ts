import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { LiteratureService } from './literature.service';
import { CreateLiteratureDto } from './dto/create-literature.dto';
import { UpdateLiteratureDto } from './dto/update-literature.dto';

@Controller('literature')
export class LiteratureController {
  constructor(private readonly literatureService: LiteratureService) {}

  @Post()
  create(@Body() createLiteratureDto: CreateLiteratureDto) {
    return this.literatureService.create(createLiteratureDto);
  }

  @Get()
  findAll() {
    return this.literatureService.findAll();
  }

  @Get('latest')
  findLatest() {
    return this.literatureService.findLatest();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.literatureService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLiteratureDto: UpdateLiteratureDto) {
    return this.literatureService.update(+id, updateLiteratureDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.literatureService.remove(+id);
  }
}
