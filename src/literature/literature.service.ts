import { Injectable } from '@nestjs/common';
import { CreateLiteratureDto } from './dto/create-literature.dto';
import { UpdateLiteratureDto } from './dto/update-literature.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Literature } from './entities/literature.entity';

@Injectable()
export class LiteratureService {
  constructor(
    @InjectRepository(Literature)
    private literatureRepository: Repository<Literature>,
  ) {}

  async create(createLiteratureDto: CreateLiteratureDto) {
    return await this.literatureRepository.save(createLiteratureDto);
  }

  async findAll() {
    const res = await this.literatureRepository
      .createQueryBuilder('lit')
      .leftJoinAndSelect('lit.user', 'user')
      .leftJoinAndSelect('lit.comments', 'comments')
      .leftJoinAndSelect('comments.user', 'commentedBy')
      .getMany();
    return res.map((v) => ({
      id: v.id,
      title: v.title,
      content: v.content,
      createdAt: v.createdAt,
      updatedAt: v.updatedAt,
      user: {
        id: v.user.id,
        username: v.user.username,
      },
      comments: v.comments.map((com) => ({
        comment: com.comment,
        commentedBy: com.user?.username,
        createdAt: com.createdAt,
      })),
    }));
  }
  async findLatest() {
    const res = await this.literatureRepository
      .createQueryBuilder('lit')
      .leftJoinAndSelect('lit.user', 'user')
      .leftJoinAndSelect('lit.comments', 'comments')
      .leftJoinAndSelect('comments.user', 'commentedBy')
      .limit(10)
      .orderBy('lit.createdAt', 'ASC')
      .getMany();
    return res.map((v) => ({
      id: v.id,
      title: v.title,
      content: v.content,
      createdAt: v.createdAt,
      updatedAt: v.updatedAt,
      user: {
        id: v.user.id,
        username: v.user.username,
      },
      comments: v.comments.map((com) => ({
        comment: com.comment,
        commentedBy: com.user?.username,
        createdAt: com.createdAt,
      })),
    }));
  }

  async findOne(id: number) {
    const v = await this.literatureRepository
      .createQueryBuilder('lit')
      .leftJoinAndSelect('lit.user', 'user')
      .leftJoinAndSelect('lit.comments', 'comments')
      .leftJoinAndSelect('comments.user', 'commentedBy')
      .where('lit.id=' + +id)
      .getOne();
    return {
      id: v.id,
      title: v.title,
      content: v.content,
      createdAt: v.createdAt,
      updatedAt: v.updatedAt,
      user: {
        id: v.user.id,
        username: v.user.username,
      },
      comments: v.comments.map((com) => ({
        comment: com.comment,
        commentedBy: com.user?.username,
        createdAt: com.createdAt,
      })),
    };
  }

  update(id: number, updateLiteratureDto: UpdateLiteratureDto) {
    return this.literatureRepository.update(id, updateLiteratureDto);
  }

  remove(id: number) {
    return this.literatureRepository.delete(id);
  }
}
