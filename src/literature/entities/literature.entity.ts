import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { LiteratureType } from '../../literature-type/entities/literature-type.entity';
import { Comment } from '../../comments/entities/comment.entity';

@Entity()
export class Literature {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  content: string;

  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: User;

  @ManyToOne(() => LiteratureType, { onDelete: 'CASCADE' })
  @JoinColumn()
  type: LiteratureType;

  @OneToMany(() => Comment, (comment) => comment.literature, {
    onDelete: 'SET NULL',
  })
  @JoinColumn()
  comments: Comment[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
