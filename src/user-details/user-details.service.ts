import { Injectable } from '@nestjs/common';
import { CreateUserDetailDto } from './dto/create-user-detail.dto';
import { UpdateUserDetailDto } from './dto/update-user-detail.dto';
import { UserDetail } from './entities/user-detail.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserDetailsService {
  constructor(
    @InjectRepository(UserDetail)
    private userDetailsRepository: Repository<UserDetail>,
  ) {}

  async findAll(): Promise<UserDetail[]> {
    return await this.userDetailsRepository.find();
  }

  async findOne(id): Promise<UserDetail> {
    return await this.userDetailsRepository.findOne(+id);
  }

  async create(createUserDetailDto: CreateUserDetailDto): Promise<UserDetail> {
    return await this.userDetailsRepository.save(createUserDetailDto);
  }

  async update(userDetail: UpdateUserDetailDto): Promise<UpdateResult> {
    return await this.userDetailsRepository.update(userDetail.id, userDetail);
  }

  async getUserDetailsByUserId(id) {
    return await this.userDetailsRepository
      .createQueryBuilder('details')
      .where('userId = ' + id)
      .innerJoinAndSelect('details.user', 'user')
      .getOne();
  }
  async remove(id): Promise<DeleteResult> {
    return await this.userDetailsRepository.delete(id);
  }
}
