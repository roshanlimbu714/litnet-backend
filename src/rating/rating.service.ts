import { Injectable } from '@nestjs/common';
import { CreateRatingDto } from './dto/create-rating.dto';
import { UpdateRatingDto } from './dto/update-rating.dto';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { Rating } from "./entities/rating.entity";

@Injectable()
export class RatingService {
  constructor(
    @InjectRepository(Rating)
    private ratingRepository: Repository<Rating>,
  ) {}

  async create(createRatingDto: CreateRatingDto) {
    return await this.ratingRepository.save(createRatingDto);
  }

  async findAll() {
    return await this.ratingRepository.find({});
  }

  findOne(id: number) {
    return this.ratingRepository.find({ id: +id });
  }

  findOneRating(id: number) {
    return this.ratingRepository
      .createQueryBuilder('rating')
      .where('rating.literature=' + +id)
      .select('AVG(rating.rate)', 'rating')
      .getRawOne();
  }

  update(id: number, updateRatingDto: UpdateRatingDto) {
    return this.ratingRepository.update(id, updateRatingDto);
  }

  remove(id: number) {
    return this.ratingRepository.delete(id);
  }
}
