import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto, toUserDto, UserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { LoginUserDto, User } from './entities/user.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { comparePasswords } from '../shared/utils';
import { UserDetailsService } from '../user-details/user-details.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private userDetailsService: UserDetailsService,
  ) {}

  async createUser(userDto): Promise<any> {
    const { username, password, email } = userDto;
    const userInDb = await this.userRepository.findOne({
      where: [{ username }, { email }],
    });

    if (userInDb) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    const { userDetail } = userDto;
    const { contact } = userDto;


    const user: any = await this.userRepository.create({
      username,
      password,
      email,
    });

    await this.userRepository.save({
      ...user,
      password: await bcrypt.hash(password, 10),
      status: 'INACTIVE',
    });
    return toUserDto(user);
  }

  async findByLogin({ username, password }: LoginUserDto): Promise<any> {
    const user = await this.userRepository.findOne({
      where: [{ username }, { email: username }],
    });

    if (!user) {
      throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
    }

    // compare passwords
    const areEqual = await comparePasswords(user.password, password);

    if (!areEqual) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }

    return toUserDto(user);
  }

  async findByPayload({ username }: any): Promise<UserDto> {
    return await this.userRepository.findOne({
      where: { username },
    });
  }

  async findAll(): Promise<any[]> {
    return await this.userRepository.createQueryBuilder('user').getMany();
  }

  async findOne(id): Promise<any> {
    return await this.userRepository
      .createQueryBuilder('user')
      .where('user.id = ' + +id)
      .getOne();
  }

  async create(userDto: CreateUserDto): Promise<UserDto> {
    const { username, password, email } = userDto;

    // check if the user exists in the db
    const userInDb = await this.userRepository.findOne({
      where: { username },
    });
    if (userInDb) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    const user: User = await this.userRepository.create({
      username,
      password,
      email,
    });
    await this.userRepository.save(user);
    return toUserDto(user);
  }

  async update(user: UpdateUserDto): Promise<UpdateResult> {
    return await this.userRepository.update(user.id, user);
  }

  async remove(id): Promise<DeleteResult> {
    return await this.userRepository.delete(id);
  }

  async getAllUserDetails(id) {
    return await this.userRepository
      .createQueryBuilder('user')
      .where('user.id = ' + id)
      .leftJoinAndSelect('user.userDetail', 'userDetail')
      .innerJoin('userDetail.role', 'role')
      .innerJoin('role.roleAuthority', 'roleAuthority')
      .innerJoin('roleAuthority.authority', 'authority')
      .addSelect([
        'user.id',
        'role.title',
        'roleAuthority.allowed',
        'authority.code',
        'authority.module',
        'authority.action',
      ])
      .getOne();
  }

  private _sanitizeUser(user: User) {
    delete user.password;
    return user;
  }
}
