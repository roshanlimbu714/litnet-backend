import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

import * as bcrypt from 'bcrypt';
import { UserDetail } from '../../user-details/entities/user-detail.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true, nullable: false })
  username: string;

  @Column()
  password: string;

  @Column({ unique: true, nullable: false })
  email: string;

  @Column()
  status: string;

  @OneToOne(() => UserDetail, { nullable: true, onDelete: 'SET NULL' })
  @JoinColumn()
  userDetail: UserDetail;

  @BeforeInsert() async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
    this.status = 'INACTIVE';
  }
}

export interface LoginStatus {
  success: boolean;
  message: string;
}

export interface LoginUserDto {
  username: string;
  password: string;
}
